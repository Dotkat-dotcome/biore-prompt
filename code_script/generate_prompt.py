
'''
shortest path in the sentence order without words not appearing on the path
'''
import os, re, math, string
import spacy
from spacy import displacy
import networkx as nx

# from sentence_transformers import SentenceTransformer
import torch
from transformers import BertTokenizer, BertModel

import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy import spatial
from collections import Counter, defaultdict
import argparse, pathlib

nlp = spacy.load("en_core_web_trf")


# annotation entity names
ents = './../data/chemprot/t_terms/ents.tsv'
ents_list = []
with open(ents, 'r') as f:
        lines = f.readlines()   
        for text in lines:
            text = text.strip('\n')
            text = text.lower()
            text = text.replace('.', ' ')
            text = text.replace(',', ' ')
            ents_list.append(text)
# ents_list = [int(s) for s in ents_list]
ents_set = set(ents_list)

directory = os.path.join(os.getcwd(), './../data/chemprot/abs/')
path_list = ['cpr_3_abs.tsv', 'cpr_4_abs.tsv', 'cpr_5_abs.tsv', 'cpr_6_abs.tsv', 'cpr_9_abs.tsv']
dd_list = []
# counter_list = []
# abs_list = []

def encode(text):
    
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    marked_text = "[CLS] " + text + " [SEP]"
    tokenized_text = tokenizer.tokenize(marked_text)
    indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_text)
    segments_ids = [1] * len(tokenized_text)
    tokens_tensor = torch.tensor([indexed_tokens])
    segments_tensors = torch.tensor([segments_ids])

    model = BertModel.from_pretrained('bert-base-uncased',
                                    output_hidden_states = True, # Whether the model returns all hidden-states.
                                    )
    # Put the model in "evaluation" mode, meaning feed-forward operation.
    model.eval()

    with torch.no_grad():

        outputs = model(tokens_tensor, segments_tensors)
        hidden_states = outputs[2]
    token_vecs = hidden_states[-2][0]
    sentence_embedding = torch.mean(token_vecs, dim=0)
    return sentence_embedding    


def get_prompt(template):
    for path in path_list:
        file_path = os.path.join(directory, path)
        template_candidate = []
        exceed = 0
        below = 0
        c_error = 0
        no_entities = 0
        all_number =0
        big = []
        small = []

        with open(file_path, 'r') as f:
            lines = f.readlines()
            template_dict = {}
            abs_dict = {}
            for text in lines:
                all_number += 1
                # import ipdb; ipdb.set_trace()
                text = text.strip('\n')
                text = text.lower()
                count = int(text.split('\t')[-1])
                text_clean = text.split('\t')[-3]
                text_clean = text_clean.replace('.', ' ')
                text_clean = text_clean.replace(',', ' ')

                text_clean = text_clean.replace('@ chemical $', '@chemical$')
                text_clean = text_clean.replace('@ gene $', '@gene$')
                text_clean = text_clean.replace('@ gene- $', '@gene$')

                # get rid of punctuation if there is any around chemical and gene
                front_check = [i-1 for i, c in enumerate(text_clean) if c == '@']
                back_check = [i+1 for i, c in enumerate(text_clean) if c == '$']
                check = front_check + back_check

                for char in check:
                    if char in range(0,len(text_clean)) and text_clean[char] in string.punctuation:
                        text_clean = text_clean[:char] + ' ' + text_clean[char+1:]

                text_clean = text_clean.replace('@chemical ', '@chemical$')
                text_clean = text_clean.replace('@gene ', '@gene$')
                text_clean = text_clean.replace('@gene-', '@gene')
                text_spaced = text_clean

                if '@chemical$' not in text_clean or '@gene$' not in text_clean:
                    no_entities += 1
                    continue   

                add_space_back_check = [i+1 for i, c in enumerate(text_spaced) if c == '$']
                # try: 
                if add_space_back_check[1] < len(text_spaced):
                    if text_spaced[add_space_back_check[0]] != " " and text_spaced[add_space_back_check[1]] != " ":
                        text_spaced = text_spaced[:add_space_back_check[0]] + " " +\
                        text_spaced[add_space_back_check[0]+1:add_space_back_check[1]] + " " + text_spaced[add_space_back_check[1]+1:]
                    
                    elif text_spaced[add_space_back_check[0]] != " " and text_spaced[add_space_back_check[1]] == " ":
                        text_spaced = text_spaced[:add_space_back_check[0]] + " " + text_spaced[add_space_back_check[0]+1:]
                        
                    elif text_spaced[add_space_back_check[0]] == " " and text_spaced[add_space_back_check[1]] != " ":
                        text_spaced = text_spaced[:add_space_back_check[1]] + " " + text_spaced[add_space_back_check[1]+1:]
                
                    # elif add_space_back_check[1] >= len(text_spaced):
                else:
                    if text_spaced[add_space_back_check[0]] != " ":
                        text_spaced = text_spaced[:add_space_back_check[0]] + " " + text_spaced[add_space_back_check[0]+1:]
                # except:
                #     print('add space back error')
                #     import ipdb; ipdb.set_trace()
                    


                add_space_front_check = [i-1 for i, c in enumerate(text_spaced) if c == '@']
                # try: 
                if add_space_front_check[0] != -1:
                    if text_spaced[add_space_front_check[0]] != " " and text_spaced[add_space_front_check[1]] != " ":
                            text_spaced = text_spaced[:add_space_front_check[0]] + " " +\
                            text_spaced[add_space_front_check[0]+1:add_space_front_check[1]] + " " + text_spaced[add_space_front_check[1]+1:]
                        
                    elif text_spaced[add_space_front_check[0]] != " " and text_spaced[add_space_front_check[1]] == " ":
                        text_spaced = text_spaced[:add_space_front_check[0]] + " " + text_spaced[add_space_front_check[0]+1:]
                        
                    elif text_spaced[add_space_front_check[0]] == " " and text_spaced[add_space_front_check[1]] != " ":
                        text_spaced = text_spaced[:add_space_front_check[1]] + " " + text_spaced[add_space_front_check[1]+1:]
                
                # elif add_space_front_check[0] == -1:
                else:
                    if text_spaced[add_space_front_check[0]] == " " and text_spaced[add_space_front_check[1]] != " ":
                        text_spaced = text_spaced[:add_space_front_check[1]] + " " + text_spaced[add_space_front_check[1]+1:]

                # get rid of extra spaces
                text_spaced = ' '.join(text_spaced.split())

            
                # Load spacy's dependency tree into a networkx graph
                doc = nlp(text_spaced)

                # with index match
                edges = []
                for token in doc:
                    for child in token.children:
                        edges.append((token.i, child.i))
                
                # with literal match
                edges_ = []
                for token in doc:
                    for child in token.children:
                        edges_.append(('{0}'.format(token.lower_),
                                    '{0}'.format(child.lower_)))
                
                graph = nx.Graph(edges)
                e1 = '@chemical$'
                e2 = '@gene$'

                # Find the shortest path
                for token in doc:
                    if e1 == token.lower_:
                        e1_index = token.i
                    if e2 == token.lower_:
                        e2_index = token.i

                try:
                    sd_path = nx.shortest_path(graph, source=e1_index, target=e2_index)
                    sd_path.sort()

                    template_raw = []
                    for i in sd_path:
                        template_raw.append(doc[i])

                    
                    if len(template_raw) > 5:
                        
                        # import ipdb; ipdb.set_trace()
                        exceed += 1
                    
                    elif len(template_raw) < 5:
                        # import ipdb; ipdb.set_trace()
                        below += 1
                    
                    # elif len(tokenizer.encode())!=5:
                    #     tok_notfit += 1
                    
                    else:
                        
                        if template == '1xxx2':
                            if str(template_raw[0]) == e1 and str(template_raw[4]) == e2:
                                if template_raw[1] not in ents_set and  template_raw[2] not in ents_set and  template_raw[3] not in ents_set:
                                    t = ' '.join([str(item) for item in template_raw])        
                                    template_candidate.append(t)
                                    abs_dict[t] = count
                            else:
                                pass

                        
                        elif template == '1xx2x':
                            if str(template_raw[0]) == e1 and str(template_raw[3]) == e2:
                                if template_raw[1] not in ents_set and  template_raw[2] not in ents_set and  template_raw[4] not in ents_set:
                                    t = ' '.join([str(item) for item in template_raw])        
                                    template_candidate.append(t)
                                    abs_dict[t] = count
                            else:
                                pass

                        
                        elif template == 'x1x2x':
                            if str(template_raw[1]) == e1 and str(template_raw[3]) == e2:
                                if template_raw[0] not in ents_set and  template_raw[2] not in ents_set and  template_raw[4] not in ents_set:
                                    t = ' '.join([str(item) for item in template_raw])        
                                    template_candidate.append(t)
                                    abs_dict[t] = count
                            else:
                                pass


                        elif template == 'x1xx2':
                            if str(template_raw[1]) == e1 and str(template_raw[4]) == e2:
                                if template_raw[2] not in ents_set and  template_raw[3] not in ents_set and  template_raw[4] not in ents_set:
                                    t = ' '.join([str(item) for item in template_raw])        
                                    template_candidate.append(t)
                                    abs_dict[t] = count
                            else:
                                pass

                        elif template == 'xx1x2':
                            if str(template_raw[2]) == e1 and str(template_raw[4]) == e2:
                                if template_raw[0] not in ents_set and  template_raw[1] not in ents_set and  template_raw[3] not in ents_set:
                                    t = ' '.join([str(item) for item in template_raw])        
                                    template_candidate.append(t)
                                    abs_dict[t] = count
                            else:
                                pass

                        
                        else:
                            print('template not in the top list')

                    
                        # template_candidate.append(t)
                    # import ipdb; ipdb.set_trace()

                except: 
                    # import ipdb; ipdb.set_trace()
                    c_error+=1
            

            # template_candidate = list(filter(None, template_candidate))
            # for the top 3 in the dictionary, we pick the top 1
            
            template_candidate = list(filter(None, template_candidate))
            counter = Counter(template_candidate)
            dd = defaultdict(list)
            for d in (abs_dict, counter): # you can list as many input dicts as you want here 
                for key, value in d.items(): 
                    dd[key].append(value)
            dd_list.append(dd)
            
            # abs_list.append(abs_dict)
            # counter_list.append(counter)


    # model = SentenceTransformer('sentence-transformers/bert-base-nli-mean-tokens')
    # module_url = "https://tfhub.dev/google/universal-sentence-encoder/4" 
    # model = hub.load(module_url)
    key_words = ["activation", "inhibition", "agonist", "antagonist", "substrate"]

    os.mkdir('./../data/chemprot/dependency/{}_5w'.format(template))
    for path, counter, key_word in zip(path_list, dd_list, key_words):
        file_path = os.path.join(directory, path)
        d_path = './../data/chemprot/dependency/{}_5w/{}'.format(template, path)
        
        with open(d_path, 'w') as output:
            for key,v in counter.items():
                abs_count = v[0]
                value = v[1]
                
                # tf
                # tf = value

                # tf-idf
                num_dict = sum(key in dict(counter) for counter in dd_list)
                tfidf = value * math.log(6/num_dict) 

                # sim
                e_key_word = encode(key_word).tolist()
                e_key = encode(key).tolist()
                sim = 1 - spatial.distance.cosine(e_key, e_key_word)
                # e_key_word = model([key_word])[0]
                # e_key = model([key])[0]
                # sim = cosine(e_key, e_key_word)

                # sim + tf-idf
                combined_1 = sim * value * math.log(6/num_dict) / abs_count
                combined_2 = sim * value * math.log(6/num_dict) * 1/(1+math.exp(abs_count))

                # regularization term
                # mutliplier, positive, downwards, 
                combined_original = sim * value * math.log(6/num_dict)

                # output.write(str(tf)+'\t'+str(tfidf)+'\t'+str(sim)+'\t'+str(combined)+'\t'+str(key)+'\n')
                output.write(str(value)+'\t'+str(abs_count)+'\t'+str(combined_1)+'\t'+str(combined_2)+'\t'+
                str(combined_original)+'\t'+str(key)+'\n')

  

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-template', type=str)
    args = parser.parse_args()
    
    template = args.template
    get_prompt(template)

                
if __name__ == '__main__':
    main()